function History(props) {
    return (
        <div className="main-cntr">
            <h2>History</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Opponent</th>
                        <th scope="col">Score</th>
                    </tr>
                </thead>
                <tbody>
                    { props.winnerList.length > 0 ? props.winnerList.map((winner, index) => (
                        <tr key={index}>
                            <th scope="row">{index+1}</th>
                            <td>{winner.name}</td>
                            <td>{winner.opponent}</td>
                            <td>{winner.value}</td>
                        </tr>
                    )) : null }
            
                </tbody>
            </table>
        </div>
    );
}

export default History;