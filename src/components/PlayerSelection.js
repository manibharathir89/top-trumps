import React, { Component } from 'react';
import constants from '../common/constants';
import people from "../data/people.json";
import starships from "../data/starships.json";

import ModuleHeader from './ModuleHeader';
import SelectionDisplay from './SelectionDisplay';
import TileDisplay from './TileDisplay';
import WinnerContainer from './WinnerContainer';
import Searchbox from './Searchbox';

class PlayerSelection extends Component {

    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.myRef1 = React.createRef();
        this.state = {
            playerArray: [ "People", "Starships"],
            playerCountArray: [ 2, 3, 4, 5 ],
            country: "",
            showPlayerDropdown: false,
            showCountDropdown: false,
            selectedPlayer: "",
            selectedCount: 0,
            displayList: [],
            winnerName: "",
            winnerNameList: []
        }
    }

    componentDidMount() {
        document.addEventListener("mousedown", this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutside);
        this.props.updateList(this.state.winnerNameList);
    }

    handleClickOutside = event => {
        const node = this.myRef.current;
        const node1 = this.myRef1.current;
        if (node && !node.contains(event.target)) {
            this.setState({
                showPlayerDropdown: false,
            });
        }
        if (node1 && !node1.contains(event.target)) {
            this.setState({
                showCountDropdown: false,
            });
        }
    };

    handleTextboxChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    onTextboxFocus = (e) => {
        if (e.target.name === 'play') {
            this.setState({
                showPlayerDropdown: true,
                showCountDropdown: false
            })
        } else {
            this.setState({
                showCountDropdown: true,
                showPlayerDropdown: false
            })
        }
    }

    updatePlayerList = (e) => {
        this.setState({
            selectedPlayer: e.target.id,
            showPlayerDropdown: false,
            selectedCount: 0
        })
    }

    updatePlayerCount = (e) => {
        this.setState({
            selectedCount: e.target.id,
            showCountDropdown: false
        })
        const selectedPlayer = this.state.selectedPlayer.toLowerCase(), dispTileArray = [], randomItems = [];
        if(selectedPlayer === "people") {
            people["results"].forEach((pl) => {
                const dispTile = {
                    name: pl.name,
                    value: pl.height
                }
                dispTileArray.push(dispTile);
            });
        } else {
            starships["results"].forEach((pl) => {
                const dispTile = {
                    name: pl.name,
                    value: pl.hyperdrive_rating
                }
                dispTileArray.push(dispTile);
            });
        }
        for( let i=0; i<e.target.id;i++) {
            var randomItem = dispTileArray[Math.floor(Math.random()*dispTileArray.length)];
            randomItems.push(randomItem);
        }
        const valueList = randomItems.map((a) => a.value)
        const maxValue = Math.max(...valueList);
        const maxObj = randomItems.filter(item => parseInt(item.value) === maxValue);
        const winnerList = [...this.state.winnerNameList];
        winnerList.push({
            name: maxObj[0].name,
            opponent: this.state.selectedPlayer,
            value: maxValue
        });
        this.setState({
            displayList: randomItems,
            winnerName: maxObj[0].name,
            winnerNameList: winnerList
        });
    }

    resetHandler = () => {
        this.setState({
            country: "",
            showPlayerDropdown: false,
            showCountDropdown: false,
            selectedPlayer: "",
            selectedCount: 0
        });
    }

    render() {
        return (
            <div className="main-cntr">
                <h2 className="mb-3">{constants.title}</h2>
                <div className='row game_cntr'>
                    <div className='col-md-2'>
                        <ModuleHeader title={constants.step1} desc={constants.playTitle} />
                        <div className="col-8 offset-2 dropdown_cntr playerDrpdwn" ref={this.myRef}>
                            <Searchbox
                                state={this.state}
                                textName="play"
                                textOnChange={this.handleTextboxChange}
                                textOnFocus={this.onTextboxFocus}
                                textOnKeyDown={this.handleKeypress}
                                isShow={this.state.showPlayerDropdown}
                                values={this.state.playerArray}
                                onSelect={this.updatePlayerList}
                                isDisabled={this.state.selectedPlayer ? true : false}
                            />
                        </div>
                        <SelectionDisplay selection={this.state.selectedPlayer} isDropdownShow={this.state.showPlayerDropdown} />
                    </div>
                    <div className='col-md-2 mb-3'>
                        {this.state.selectedPlayer ?
                            <div>
                                <ModuleHeader title={constants.step2} desc={constants.selectPlayers} />
                                <div className="col-8 offset-2 dropdown_cntr countryDrpdwn" ref={this.myRef1}>
                                    <Searchbox
                                        state={this.state}
                                        textName="playerCount"
                                        textOnChange={this.handleTextboxChange}
                                        textOnFocus={this.onTextboxFocus}
                                        textOnKeyDown={null}
                                        isShow={this.state.showCountDropdown}
                                        values={this.state.playerCountArray}
                                        onSelect={this.updatePlayerCount}
                                        isDisabled={this.state.selectedCount ? true : false}
                                    />
                                </div>
                                <SelectionDisplay selection={this.state.selectedCount} isDropdownShow={this.state.showCountDropdown} />
                            </div>
                        : null
                        }
                    </div>
                    <div className='col-md-6 mb-3'>
                        {this.state.selectedCount ?
                            <TileDisplay tileCount={this.state.selectedCount} dispList={this.state.displayList} />
                            : null
                        }
                    </div>
                    <div className='col-md-2 winner_cntr col-sm'>
                        <WinnerContainer winnerName={this.state.winnerName} playersCount={this.state.selectedCount} reset={this.resetHandler} />
                    </div>
                </div>
            </div>
        );
    }

}

export default PlayerSelection;