import React from 'react';

function ModuleHeader(props) {
    return (
        <div>
            <h5 className="mb-3">{props.title}</h5>
            <p className="mb-3">{props.desc}</p>
        </div>
    )
}

export default ModuleHeader;