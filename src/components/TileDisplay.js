function TileDisplay(props) {
    return (
        props.dispList.length > 0 ?
            <div className='row tile p-0'>
                { props.dispList ? props.dispList.map((tile, index) => (
                    <div className='col-md-4 m-0 mb-3 p-0' key={index}>
                        <div className="mr-2 p-3">
                            <h5 className="mb-3">{tile.name}</h5>
                            <h4 className="mb-3">{tile.value}</h4>
                        </div>
                    </div>
                )) : null }
            </div>
        : null
    );
    
}

export default TileDisplay;