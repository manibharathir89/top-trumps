import React from 'react';
import constants from '../common/constants'

function SelectionDisplay(props) {
    return (
        props.selection && !props.isDropdownShow ?
            <div>
                <p className="mb-3 mt-4">{constants.selected} : </p>
                <h5>{props.selection}</h5>
            </div>
        : null
    )
}

export default SelectionDisplay;