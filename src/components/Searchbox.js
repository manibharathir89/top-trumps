import React, { Component } from 'react';

class Searchbox extends Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
    }

    render() {
        return (
            <div ref={this.myRef}>
                <input
                    type="text"
                    className="form-control"
                    name={this.props.textName}
                    onChange={this.props.textOnChange}
                    onFocus={this.props.textOnFocus}
                    disabled={this.props.isDisabled}
                />
                    {this.props.isShow ?
                        <ul>
                            {this.props.values.map((filteredValue, key) => (
                                <li key={key} id={filteredValue} onClick={this.props.onSelect}>
                                    {filteredValue}
                                </li>
                            ))}
                        </ul>
                    : null }
                </div>
        );
    }
}

export default Searchbox;