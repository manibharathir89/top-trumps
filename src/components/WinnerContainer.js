import constants from '../common/constants';

function WinnerContainer(props) {
    return (
        props.playersCount ?
            <div>
                <h5 className="mb-3">{constants.winner} : </h5>
                <p className="mb-3">{props.winnerName}</p>
                <button onClick={props.reset}>{constants.restart}</button>
            </div>
        : null
    );
    
}

export default WinnerContainer;