import React from 'react';
import './App.css';
import ErrorBoundary from "./common/ErrorBoundary";
import RoutesC from './common/routes';

import 'bootstrap/dist/css/bootstrap.min.css';
import './stylesheet/styles.css';

function App() {
    return (
        <div className="App">
            <ErrorBoundary>
                <RoutesC />
            </ErrorBoundary>
        </div>
    );
}

export default App;