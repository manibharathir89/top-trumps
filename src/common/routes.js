import React from "react";
import { Route, Link, Switch } from "react-router-dom";
import History from "../components/History";
import PlayerSelection from "../components/PlayerSelection";

class Routes extends React.Component {
    state = {
        winnerNameList: [],
    };

    setWinnerList = (list) => {
        const winnerList = [...this.state.winnerNameList,...list];
        this.setState({
            winnerNameList: winnerList
        })
    }

    render() {
        return (
            <React.Fragment>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <Link className="mr-3" exact="true" to="/">
                        Play Game
                    </Link>
                    <Link exact="true" to="/history">
                        View History
                    </Link>
                </nav>
                <Switch>
                    <Route path="/history" component={
                        () => <History winnerList={this.state.winnerNameList} />
                    } />
                    <Route path="/" component={
                        () => <PlayerSelection updateList={this.setWinnerList}/>
                    } />
                </Switch>
            </React.Fragment>
        );
    }
}

export default Routes;