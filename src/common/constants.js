const Constants =  {
    title: "Star Wars Top Trumps",
    step1: 'Step 1',
    playTitle: 'Play against',
    selected: 'You selected',
    step2: 'Step 2',
    selectPlayers: 'No. of Players',
    winner: 'Winner',
    winnerSubtext: 'Winner Name',
    restart: "Restart Game"
}

export default Constants;